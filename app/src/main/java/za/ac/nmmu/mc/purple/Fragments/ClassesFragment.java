package za.ac.nmmu.mc.purple.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.TextViewAction;
import com.dexafree.materialList.card.action.WelcomeButtonAction;
import com.dexafree.materialList.view.MaterialListView;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Activities.AddClassActivity;
import za.ac.nmmu.mc.purple.Activities.ChatActivity;
import za.ac.nmmu.mc.purple.Activities.ClassDetailsActivity;
import za.ac.nmmu.mc.purple.Activities.HotspotDetailsActivity;
import za.ac.nmmu.mc.purple.Activities.MainActivity;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.Hotspot;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.Models.User;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

/**
 * Created by danie on 4/25/2016.
 */
public class ClassesFragment extends Fragment {
    private boolean FreshPull = true;
    private SwipeRefreshLayout swipeContainer;
    private MaterialListView mListView;
    public ProgressDialog progressDialog;
    private View rootView;
    public ClassesFragment() {
    }
    public static ClassesFragment newInstance() {
        ClassesFragment fragment = new ClassesFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main_classes, container, false);

        FloatingActionButton fab  = (FloatingActionButton) rootView.findViewById(R.id.class_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity().getBaseContext(), AddClassActivity.class));
            }
        });
        mListView = (MaterialListView) rootView.findViewById(R.id.class_material_listview);
        progressDialog = new ProgressDialog(getActivity(),R.style.AppTheme_Dark_Dialog);

        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.class_swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetclassList();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        if(AppToolbox.user!=null)
            GetclassList();
        return rootView;
    }
    private void showAsynchDialog(String message){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
    private void dismissAsynchDialog(){
        progressDialog.dismiss();
    }
    private void GetclassList(){
        if(FreshPull)
            showAsynchDialog("Fetching....");
        swipeContainer.setRefreshing(true);
        AppToolbox.service.getClasses(AppToolbox.user.UserID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Classes>>() {
                    @Override
                    public void onCompleted() {
                        swipeContainer.setRefreshing(false);
                        dismissAsynchDialog();
                        FreshPull = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        swipeContainer.setRefreshing(false);
                        SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.general_failure), SuperToast.Duration.LONG,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                    }

                    @Override
                    public void onNext(ArrayList<Classes> classlist) {
                        mListView.getAdapter().clearAll();
                        if (classlist.size() != 0) {
                            UpdateList(classlist);
                        } else {
                            SuperToast.create(getActivity().getBaseContext(), getResources().getString(R.string.alert_empty_classlist), SuperToast.Duration.LONG,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                        }
                    }
                });

    }

    public void UpdateList(ArrayList<Classes> classlist){

        for(int i = 0; i < classlist.size(); i++)
        {
            final Classes currentClass = classlist.get(i);
            String ClassName = currentClass.getClassName();
            String ClassInstitution = currentClass.getClassInstitution();
            String ClassDescription = currentClass.getClassDescription();

            if(ClassDescription.length() > 100) {
                ClassDescription = ClassDescription.substring(0, 100).concat("...");
            }
            Card DetailsCard = new Card.Builder(getContext())
                    .withProvider(new CardProvider())
                    .setLayout(R.layout.class_material_details_card_layout)
                    .setTitle(ClassName)
                    .setTitleColor(Color.WHITE)
                    .setDescription(ClassDescription)
                    .setDescriptionColor(Color.WHITE)
                    .setSubtitle(ClassInstitution)
                    .setSubtitleColor(Color.WHITE)
                    .setBackgroundColor(Color.rgb(0, 150, 136))
                    .addAction(R.id.ok_button, new WelcomeButtonAction(getContext())
                            .setText("Details")
                            .setTextColor(Color.WHITE)
                            .setListener(new OnActionClickListener() {
                                @Override
                                public void onActionClicked(View view, Card card) {
                                    Intent DetailsIntent = new Intent(getActivity().getBaseContext(), ClassDetailsActivity.class);
                                    DetailsIntent.putExtra("CC", currentClass);
                                    startActivity(DetailsIntent);
                                }
                            }))
                    .endConfig()
                    .build();
            mListView.getAdapter().add(DetailsCard);
        }

    }

}
