package za.ac.nmmu.mc.purple.Models;

import java.io.Serializable;
import java.util.UUID;

import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

/**
 * Created by danie on 4/22/2016.
 */
public class User implements Serializable{
    public String UserID;
    private String FullName;
    public String CellNumber;
    public int Karma;

    public User(){}
    public User(String FullName, String CellNumber)
    {
        this.UserID = UUID.randomUUID().toString();
        this.FullName = AppToolbox.Encode64(FullName);
        this.CellNumber = CellNumber;
        this.Karma = 0;
    }

    public String getFullName() {
        return AppToolbox.Decode64(FullName);
    }

    @Override
    public String toString() {
        return "@"+getFullName()+" (K: "+Karma+") (C: "+CellNumber+")";
    }
}
