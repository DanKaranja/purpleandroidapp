package za.ac.nmmu.mc.purple.WebApi;


import retrofit2.http.*;
import rx.Observable;
import za.ac.nmmu.mc.purple.Models.ChatMessage;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.Hotspot;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.Models.User;

import java.util.ArrayList;

/**
 * Created by danie on 4/2/2016.
 */
public interface RestInterface {
    //Methods related to user functions

    @POST("Login")
    Observable<User> loginUser(@Body String[] auth);

    @POST("Register")
    Observable<PResponse> registerUser(@Body User user);

    @GET("Logout/{userID}")
    Observable<PResponse> deleteUser(@Path("userID") String userID);

    //Methods related to the class activities
    @GET("SearchClass/{userID}")
    Observable<ArrayList<Classes>> getClasses(@Path("userID") String userID);

    @POST("SearchClass")
    Observable<PResponse> searchClass(@Body String[] classDetails);

    @POST("AddClass")
    Observable<PResponse> createClass(@Body Classes newClass);

    @POST("JoinClass")
    Observable<PResponse> joinClass(@Body String[] classUserDetails);

    @POST("LeaveClass")
    Observable<PResponse> leaveClass(@Body String[] classUserDetails);

    @GET("GetClassUsers/{classID}")
    Observable<ArrayList<User>> getClassUsers(@Path("classID") String classID);

    //Methods related to the Hotspot activities
    @POST("CreateHotspot")
    Observable<PResponse> createHotspot(@Body Hotspot newHotspot);

    @GET("GetHotspots/{userID}")
    Observable<ArrayList<Hotspot>> getHotspots(@Path("userID") String userID);

    @GET("GetHotspotUsers/{hotspotID}")
    Observable<ArrayList<User>> getHotspotUsers(@Path("hotspotID") String hotspotID);

    @POST("LeaveHotspot")
    Observable<PResponse> leaveHotspot(@Body String[] hotspotUserDetails);

    //Methods related to the Chat Activity
    @GET("User/{userID}")
    Observable<User> getUser(@Path("userID") String userID);

    @POST("SendMsg")
    Observable<PResponse> sendChatMsg(@Body ChatMessage chatMsg);

    @POST("Vote")
    Observable<PResponse> sendVote(@Body String[] vote);

    @GET("GetAllMsg/{hotspotID}")
    Observable<ArrayList<ChatMessage>> getAllHotspotMsg(@Path("hotspotID") String hotspotID);




}
