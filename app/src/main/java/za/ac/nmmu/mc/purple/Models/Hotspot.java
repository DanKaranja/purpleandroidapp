package za.ac.nmmu.mc.purple.Models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

/**
 * Created by danie on 4/25/2016.
 */
public class Hotspot implements Serializable{
    public String HotspotID;
    private String HotspotQuestion;
    private String HotspotDescription;
    public String HotspotAdminUserID;
    public String HotspotCreationDate;
    public ArrayList<String> HotspotClassIDs;

    public Hotspot() { }
    public Hotspot(String Title,String Description, String AdminUserID, String CreationDate, ArrayList<String> ClassIDs)
    {
        this.HotspotID = UUID.randomUUID().toString();
        this.HotspotQuestion = AppToolbox.Encode64(Title);
        this.HotspotDescription = AppToolbox.Encode64(Description);
        this.HotspotAdminUserID = AdminUserID;
        this.HotspotCreationDate = CreationDate;
        this.HotspotClassIDs = ClassIDs;
    }

    public String getHotspotQuestion() {
        return AppToolbox.Decode64(HotspotQuestion);
    }

    public String getHotspotDescription() {
        return AppToolbox.Decode64(HotspotDescription);
    }
}
