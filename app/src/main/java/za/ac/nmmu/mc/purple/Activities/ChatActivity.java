package za.ac.nmmu.mc.purple.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.mlsdev.rximagepicker.RxImageConverters;
import com.mlsdev.rximagepicker.RxImagePicker;
import com.mlsdev.rximagepicker.Sources;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Models.ChatMessage;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.Utilities.ScalingUtilities;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

public class ChatActivity extends AppCompatActivity {

    private int screenWidth;
    private int screenHeight;
    private boolean FreshPull = true;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FloatingActionButton _chat_send_fab;
    private TextView _chat_msg_textview;
    private ChatAdapter mAdapter;
    private SwipeRefreshLayout swipeContainer;
    public ProgressDialog progressDialog;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (AppToolbox.CHID == null) {
            SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), SuperToast.Duration.LONG,
                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
            finish();
        }
        setContentView(R.layout.activity_chat);
        screenWidth = getResources().getDisplayMetrics().widthPixels;
        screenHeight = getResources().getDisplayMetrics().heightPixels;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(ChatActivity.this,R.style.AppTheme_Dark_Dialog);
        mRecyclerView = (RecyclerView) findViewById(R.id.chat_recycler);
        _chat_send_fab = (FloatingActionButton) findViewById(R.id.chat_send_fab);
        _chat_msg_textview = (TextView) findViewById(R.id.chat_msg_textview);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getBaseContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ChatAdapter();
        mRecyclerView.setAdapter(mAdapter);
        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.chat_swipeContainer);
        // Setup refresh listener which triggers new data loading
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetAllMsg();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        _chat_send_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = _chat_msg_textview.getText().toString();
                if (message.length() != 0)
                    SendMsg(new ChatMessage(message, AppToolbox.user.UserID, AppToolbox.CHID, new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(Calendar.getInstance().getTime()), 0));
                HideKeyboard();
                _chat_msg_textview.clearFocus();
                _chat_msg_textview.getEditableText().clear();

            }
        });
        //Initially getting thread;
        if(!(AppToolbox.CHATCACHE.containsKey(AppToolbox.CHID)) || (AppToolbox.CHATCACHE.get(AppToolbox.CHID)==null)) {
            GetAllMsg();
        }
        else {
            mAdapter.addAll(AppToolbox.CHATCACHE.get(AppToolbox.CHID));
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
    private void showAsynchDialog(String message){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
    private void dismissAsynchDialog(){
        progressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_chat, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_upload_camera) {
            GetImgCamera();
            return true;
        }
        if (id == R.id.action_upload_gallery) {
            GetImgGallery();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private void GetImgCamera(){
        RxImagePicker.with(getBaseContext()).requestImage(Sources.CAMERA)
                .flatMap(new Func1<Uri, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Uri uri) {
                        return RxImageConverters.uriToBitmap(getBaseContext(), uri);
                    }
                })
                .subscribe(new Action1<Bitmap>() {
                    @Override
                    public void call(Bitmap bitmap) {
                        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                            // Notice that width and height are reversed
                            Bitmap scaled = ScalingUtilities.createScaledBitmap(bitmap, screenHeight, screenWidth, ScalingUtilities.ScalingLogic.FIT);
                            int w = scaled.getWidth();
                            int h = scaled.getHeight();
                            // Setting post rotate to 90
                            Matrix mtx = new Matrix();
                            mtx.postRotate(90);
                            // Rotating Bitmap
                            bitmap = Bitmap.createBitmap(scaled, 0, 0, w, h, mtx, true);
                        }else{// LANDSCAPE MODE
                            //No need to reverse width and height
                            Bitmap scaled = ScalingUtilities.createScaledBitmap(bitmap, screenHeight, screenWidth, ScalingUtilities.ScalingLogic.FIT);
                            bitmap=scaled;
                        }
                        PostImage(bitmap, 75);
                    }
                });
    }

    private void GetImgGallery() {
        RxImagePicker.with(getBaseContext()).requestImage(Sources.GALLERY)
                .flatMap(new Func1<Uri, Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call(Uri uri) {
                        return RxImageConverters.uriToBitmap(getBaseContext(), uri);
                    }
                })
                .subscribe(new Action1<Bitmap>() {
                    @Override
                    public void call(Bitmap bitmap) {
                        Bitmap scaled = ScalingUtilities.createScaledBitmap(bitmap, screenHeight, screenWidth, ScalingUtilities.ScalingLogic.FIT);
                        bitmap=scaled;
                        PostImage(bitmap,75);
                    }
                });

    }

    private void PostImage(Bitmap image, int compression) {
        SendMsg(new ChatMessage(AppToolbox.encodeImgToBase64(image,Bitmap.CompressFormat.JPEG,compression),AppToolbox.user.UserID, AppToolbox.CHID, new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(Calendar.getInstance().getTime()),1));
    }

    private void HideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void SendMsg(ChatMessage message) {
        swipeContainer.setRefreshing(true);
        AppToolbox.service.sendChatMsg(message)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        swipeContainer.setRefreshing(false);
                        GetAllMsg();
                    }

                    @Override
                    public void onError(Throwable e) {
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), SuperToast.Duration.LONG,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                    }

                    @Override
                    public void onNext(PResponse pResponse) {
                        if (pResponse.getCheck()) {
                            SuperToast.create(getBaseContext(), "Sent!", 800,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                        } else {
                            SuperToast.create(getBaseContext(), pResponse.getResponseString(), 800,
                                    Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                        }
                    }
                });

    }

    private void GetAllMsg() {
        if(FreshPull)
            showAsynchDialog("Fetching....");
        swipeContainer.setRefreshing(true);
        AppToolbox.service.getAllHotspotMsg(AppToolbox.CHID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<ChatMessage>>() {
                    @Override
                    public void onCompleted() {
                        swipeContainer.setRefreshing(false);
                        dismissAsynchDialog();
                        FreshPull = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), SuperToast.Duration.LONG,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();

                    }

                    @Override
                    public void onNext(ArrayList<ChatMessage> chatMessages) {
                        mAdapter.clear();
                        if (chatMessages.size() != 0) {
                            AppToolbox.CHATCACHE.put(AppToolbox.CHID,chatMessages);
                            mAdapter.addAll(chatMessages);
                        }
                    }
                });

    }


    public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
        private ArrayList<ChatMessage> mItems;

        public ChatAdapter() {
            super();
            mItems = new ArrayList<>();
        }

        public void addAll(ArrayList<ChatMessage> chatMessages) {
            mItems.addAll(chatMessages);
            notifyDataSetChanged();
        }

        public void clear() {
            mItems.clear();
            notifyDataSetChanged();
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.recycler_chat, viewGroup, false);
            ViewHolder viewHolder = new ViewHolder(v);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(ViewHolder viewHolder, int i) {
            final ChatMessage chatMessage = mItems.get(i);
            FrameLayout.LayoutParams Framparams = (FrameLayout.LayoutParams)viewHolder._chat_cardview.getLayoutParams();
            LinearLayout.LayoutParams Linparams = (LinearLayout.LayoutParams)viewHolder._chat_userDetails.getLayoutParams();
            LinearLayout.LayoutParams IVLinparams = (LinearLayout.LayoutParams)viewHolder._chat_imageView.getLayoutParams();
            if (chatMessage.UserID.contentEquals(AppToolbox.user.UserID)) {
                Framparams.gravity = Gravity.RIGHT;
                Linparams.gravity = Gravity.RIGHT;
                viewHolder._up_fab.setVisibility(View.INVISIBLE);
                viewHolder._down_fab.setVisibility(View.INVISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    viewHolder._chat_message.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_END);
                }
            }else{
                Framparams.gravity = Gravity.LEFT;
                Linparams.gravity = Gravity.LEFT;
                viewHolder._up_fab.setVisibility(View.VISIBLE);
                viewHolder._down_fab.setVisibility(View.VISIBLE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                    viewHolder._chat_message.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                }

            }
            viewHolder._chat_cardview.setLayoutParams(Framparams);
            viewHolder._chat_message.setLayoutParams(Linparams);
            viewHolder._chat_userDetails.setLayoutParams(Linparams);
            viewHolder._chat_timestamp.setLayoutParams(Linparams);
            viewHolder._chat_userDetails.setText("@" + chatMessage.FromUser.getFullName() + "( Karma: " + chatMessage.FromUser.Karma + ")");
            viewHolder._chat_timestamp.setText(chatMessage.CreationDate);
            switch(chatMessage.Type){

                case 0:
                    viewHolder._chat_message.setText(chatMessage.getMessage());
                    viewHolder._chat_imageView.setImageResource(android.R.color.transparent);
                    break;
                case 1:
                    viewHolder._chat_message.setText(null);
                    int screenWidth = (getResources().getDisplayMetrics().widthPixels)/2;
                    int screenHeight = (getResources().getDisplayMetrics().heightPixels)/3;
                    Bitmap scaled = ScalingUtilities.createScaledBitmap(chatMessage.getImgMessage(), screenWidth, screenHeight, ScalingUtilities.ScalingLogic.CROP);
                    viewHolder._chat_imageView.setImageBitmap(scaled);
                    break;


            }

            viewHolder._chat_imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent ViewImageIntent = new Intent(getBaseContext(), ImageViewerActivity.class);
                    ViewImageIntent.putExtra("MSG",chatMessage);
                    startActivity(ViewImageIntent);
                }
            });

            viewHolder._up_fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chatMessage.FromUser.UserID != null) {
                        if ((!chatMessage.UserID.contentEquals(AppToolbox.user.UserID)))
                            Vote(new String[]{chatMessage.MessageID, AppToolbox.user.UserID, chatMessage.UserID, "1"});
                    } else {
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.alert_empty_nullUser), 800,
                                Style.getStyle(Style.ORANGE, SuperToast.Animations.POPUP)).show();
                    }

                }
            });
            viewHolder._down_fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (chatMessage.FromUser.UserID != null) {
                        if ((!chatMessage.UserID.contentEquals(AppToolbox.user.UserID)))
                            Vote(new String[]{chatMessage.MessageID, AppToolbox.user.UserID, chatMessage.UserID, "-1"});
                    } else {
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.alert_empty_nullUser), 800,
                                Style.getStyle(Style.ORANGE, SuperToast.Animations.POPUP)).show();
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            public TextView _chat_userDetails;
            public TextView _chat_timestamp;
            public TextView _chat_message;
            public FloatingActionButton _up_fab;
            public FloatingActionButton _down_fab;
            public ImageView _chat_imageView;
            public CardView _chat_cardview;

            public ViewHolder(View itemView) {
                super(itemView);
                _chat_userDetails = (TextView) itemView.findViewById(R.id.chat_userDetails);
                _chat_timestamp = (TextView) itemView.findViewById(R.id.chat_timestamp);
                _chat_message = (TextView) itemView.findViewById(R.id.chat_message);
                _up_fab = (FloatingActionButton) itemView.findViewById(R.id.up_fab);
                _down_fab = (FloatingActionButton) itemView.findViewById(R.id.down_fab);
                _chat_imageView = (ImageView) itemView.findViewById(R.id.chat_imageView);
                _chat_cardview = (CardView) itemView.findViewById(R.id.chat_cardview);
            }
        }

        private void Vote(String[] Vote) {
            AppToolbox.service.sendVote(Vote)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<PResponse>() {
                        @Override
                        public void onCompleted() {
                            GetAllMsg();
                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(PResponse pResponse) {
                            if (pResponse.getCheck()) {
                                SuperToast.create(getBaseContext(), pResponse.getResponseString(), 800,
                                        Style.getStyle(Style.BLUE, SuperToast.Animations.POPUP)).show();
                            } else {
                                SuperToast.create(getBaseContext(), pResponse.getResponseString(), 800,
                                        Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                            }

                        }
                    });
        }
    }

}
