package za.ac.nmmu.mc.purple.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.Hotspot;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

public class AddHotspotActivity extends AppCompatActivity {

    public ArrayAdapter<Classes> spinnerAdapter;
    public SelectedClassesAdapter selectedListAdapter;
    public ArrayList<Classes> selectedClasses;
    public ArrayList<Classes> currentClasses;
    public CardView _hotspot_currentClasses_cardview;
    private FloatingActionButton addClass_fab;
    private FloatingActionButton _create_hotspot_Btn;
    private TextView _hotspot_title;
    private TextView _hotspot_description;
    public ListView selectedClassesList;
    public Spinner classSpinner;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_hotspot);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //views
        _hotspot_currentClasses_cardview = (CardView) findViewById(R.id.hotspot_currentClasses_cardview);
        progressDialog = new ProgressDialog(AddHotspotActivity.this,R.style.AppTheme_Dark_Dialog);
        _hotspot_title = (TextView) findViewById(R.id.hotspot_title);
        _hotspot_description = (TextView) findViewById(R.id.hotspot_description);
        classSpinner = (Spinner) findViewById(R.id.select_class_Spinner);
        selectedClassesList = (ListView) findViewById(R.id.class_selected_list);
        addClass_fab = (FloatingActionButton) findViewById(R.id.class_selector_fab);
        _create_hotspot_Btn = (FloatingActionButton) findViewById(R.id.create_hotspot_Btn);

        //meta tools
        selectedClasses = new ArrayList<>();
        currentClasses = new ArrayList<>();
        spinnerAdapter = new ArrayAdapter<Classes>(this, R.layout.simple_custom_list_item, R.id.simple_spinner_item, currentClasses);
        selectedListAdapter = new SelectedClassesAdapter(this,R.layout.simple_custom_list_item_16dp,selectedClasses);

        selectedClassesList.setAdapter(selectedListAdapter);
        classSpinner.setAdapter(spinnerAdapter);

        addClass_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Classes temp = (Classes) classSpinner.getSelectedItem();
                if (checkExisting(temp.ClassID))
                    SuperToast.create(getBaseContext(),getResources().getString(R.string.alert_selected_existing), 500,
                            Style.getStyle(Style.ORANGE, SuperToast.Animations.POPUP)).show();
                else {
                    selectedClasses.add(temp);
                    RelativeLayout.LayoutParams Cardparams = (RelativeLayout.LayoutParams) _hotspot_currentClasses_cardview.getLayoutParams();
                    int currentHeigh = Cardparams.height;
                    Cardparams.height = currentHeigh + 85;
                    _hotspot_currentClasses_cardview.setLayoutParams(Cardparams);
                }
                selectedListAdapter.notifyDataSetChanged();
            }
        });

        _create_hotspot_Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    ArrayList<String> classIDs = new ArrayList<String>();
                    for(Classes eachClass : selectedClasses){
                        classIDs.add(eachClass.ClassID);
                    }
                    Hotspot newHotspot = new Hotspot(_hotspot_title.getText().toString(),_hotspot_description.getText().toString(),AppToolbox.user.UserID,new SimpleDateFormat("dd-MMM-yyyy").format(Calendar.getInstance().getTime()),classIDs);
                    CreateHotspot(newHotspot);
                }
            }
        });

        selectedClassesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedClasses.remove(position);
                selectedListAdapter.notifyDataSetChanged();
                RelativeLayout.LayoutParams Cardparams = (RelativeLayout.LayoutParams) _hotspot_currentClasses_cardview.getLayoutParams();
                int currentHeigh = Cardparams.height;
                Cardparams.height = currentHeigh - 85;
                _hotspot_currentClasses_cardview.setLayoutParams(Cardparams);
                Snackbar.make(findViewById(R.id.addHotspot_main_layout), R.string.alert_general_item_removed, Snackbar.LENGTH_SHORT).show();
                return true;
            }
        });

        //fetching classlist
        GetclassList();

    }
    private boolean checkExisting(String classID){
        for (Classes each : selectedClasses) {
            if(each.ClassID == classID)
                return true;
        }
        return false;
    }
    private void showAsynchDialog(String message){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
    private void dismissAsynchDialog(){
        progressDialog.dismiss();
    }
    private void GoBackToMain(){
        Intent MAintent = new Intent(this, MainActivity.class);
        MAintent.putExtra("WUWMTD",1);
        startActivity(MAintent);
        finish();
    }
    private void GetclassList(){
        showAsynchDialog("Fetching data, please wait...");
        AppToolbox.service.getClasses(AppToolbox.user.UserID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Classes>>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 1500,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();

                    }

                    @Override
                    public void onNext(ArrayList<Classes> classlist) {
                        if (classlist.size() != 0) {
                            spinnerAdapter.addAll(classlist);
                        } else {
                            SuperToast.create(getBaseContext(), getResources().getString(R.string.alert_empty_classlist), 1500,
                                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                            finish();
                        }
                    }
                });

    }
    private void CreateHotspot(final Hotspot newOne){
        showAsynchDialog("Attempting to create hotspot, please wait...");
        AppToolbox.service.createHotspot(newOne)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 1500,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                    }
                    @Override
                    public void onNext(PResponse pResponse) {
                        if(pResponse.getCheck()){
                            SuperToast.create(getBaseContext(), "Successfully added "+newOne.getHotspotQuestion(), 1500,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            GoBackToMain();
                        }
                        else{
                            SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 1500,
                                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                        }
                    }
                });

    }
    public boolean validate() {
        boolean valid = true;

        String name = _hotspot_title.getText().toString();
        String description = _hotspot_description.getText().toString();

        if ((name.length() < 5) || (name.length() > 80)) {
            _hotspot_title.setError("Please stick within the character constraints");
            valid = false;
        } else {
            _hotspot_title.setError(null);
        }

        if ((description.length() < 5) || (description.length() > 250)) {
            _hotspot_description.setError("Please stick within the character constraints");
            valid = false;
        } else {
            _hotspot_description.setError(null);
        }
        if(selectedClasses.isEmpty()){
            SuperToast.create(getBaseContext(), getResources().getString(R.string.alert_empty__selected_classlist), 1500,
                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
            valid=false;
        }

        return valid;
    }
    private class SelectedClassesAdapter extends ArrayAdapter {

        private Context mContext;
        private int layoutResourceId;
        private TextView classDetials;
        ArrayList<Classes> data;


        public SelectedClassesAdapter(Context context, int layout, ArrayList<Classes> objects) {
            super(context, layout, objects);
            this.mContext = context;
            this.layoutResourceId = layout;
            this.data = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId,parent,false);
            }
            Classes currentClass = data.get(position);
            classDetials = (TextView) convertView.findViewById(R.id.simple_spinner_item);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                classDetials.setTextAlignment(View.TEXT_ALIGNMENT_TEXT_START);
                classDetials.setPadding(3,3,3,3);
            }

            classDetials.setText(currentClass.toString());

            return convertView;

        }

    }

}
