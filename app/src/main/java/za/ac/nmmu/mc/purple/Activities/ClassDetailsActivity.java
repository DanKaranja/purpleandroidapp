package za.ac.nmmu.mc.purple.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.WelcomeButtonAction;
import com.dexafree.materialList.card.provider.ListCardProvider;
import com.dexafree.materialList.view.MaterialListView;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.Models.User;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

public class ClassDetailsActivity extends AppCompatActivity {
    private MaterialListView mListView;
    private boolean FreshPull = true;
    public ProgressDialog progressDialog;
    private Classes currentClass;
    private LeaveClassDialogFragment leaveClassDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_class_details);

        Intent landingIntent = getIntent();
        currentClass = (Classes) landingIntent.getSerializableExtra("CC");
        if(currentClass == null){
            finish();
        }

        mListView = (MaterialListView) findViewById(R.id.class_details_material_listview);
        progressDialog = new ProgressDialog(ClassDetailsActivity.this,R.style.AppTheme_Dark_Dialog);
        GetClassUsers();
    }
    private void showAsynchDialog(String message){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
    private void dismissAsynchDialog(){
        progressDialog.dismiss();
    }

    private void Update(ArrayList<User> userList){
        mListView.getAdapter().clearAll();
        String Title = currentClass.getClassName();
        String Description = currentClass.getClassDescription();

        Card DetailsCard = new Card.Builder(this)
                .withProvider(new CardProvider())
                .setLayout(R.layout.general_material_details_card_layout)
                .setTitle(Title)
                .setTitleColor(Color.WHITE)
                .setDescription(Description)
                .setDescriptionColor(Color.WHITE)
                .setSubtitle(currentClass.getClassInstitution())
                .setSubtitleColor(Color.WHITE)
                .setBackgroundColor(Color.rgb(0, 150, 136))
                .addAction(R.id.ok_button, new WelcomeButtonAction(this)
                        .setText("Ditch")
                        .setTextColor(Color.WHITE)
                        .setListener(new OnActionClickListener() {
                            @Override
                            public void onActionClicked(View view, Card card) {
                                leaveClassDialogFragment = new LeaveClassDialogFragment();
                                leaveClassDialogFragment.show(getSupportFragmentManager(), "LEAVECLASS");
                            }
                        }))
                .endConfig()
                .build();
        Card UsersCard = new Card.Builder(this)
                .setTag("USERS")
                .setDismissible()
                .withProvider(new ListCardProvider())
                .setLayout(R.layout.material_list_card_layout)
                .setTitle("Students")
                .setAdapter(new UsersListAdapter(this, R.layout.simple_custom_list_item, userList))
                .endConfig()
                .build();
        mListView.getAdapter().add(DetailsCard);
        mListView.getAdapter().add(UsersCard);


    }

    private void GetClassUsers(){
        if(FreshPull)
            showAsynchDialog("Fetching....");
        AppToolbox.service.getClassUsers(currentClass.ClassID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<User>>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 500,
                                Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                    }

                    @Override
                    public void onNext(ArrayList<User> userList) {
                        if (userList.size() != 0) {
                            Update(userList);
                        } else {
                            SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 800,
                                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                        }
                    }
                });

    }

    public void RemoveClass(String[] removeDetails){
        showAsynchDialog("Ditching class");
        AppToolbox.service.leaveClass(removeDetails)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();
                        finish();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 500,
                                Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                    }

                    @Override
                    public void onNext(PResponse pResponse) {
                        SuperToast.create(getBaseContext(), pResponse.getResponseString(), 800,
                                Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    }
                });

    }
    @SuppressLint("ValidFragment")
    public class LeaveClassDialogFragment extends DialogFragment {
        private String[] removeDetails;

        public LeaveClassDialogFragment(){
            removeDetails = new String[]{AppToolbox.user.UserID,currentClass.ClassID};
        }
        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);

        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.AppTheme_Dark_Dialog);
            builder.setTitle("Leaving class.");
            builder.setMessage("Are you sure you'd like to leave "+currentClass.getClassName()+" ?")
                    .setPositiveButton(R.string.action_general_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            RemoveClass(removeDetails);
                        }
                    })
                    .setNegativeButton(R.string.action_general_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            return builder.create();
        }
    }

    private class UsersListAdapter extends ArrayAdapter {

        private Context mContext;
        private int layoutResourceId;
        private TextView Details;
        ArrayList<User> data;


        public UsersListAdapter(Context context, int layout, ArrayList<User> objects) {
            super(context, layout, objects);
            this.mContext = context;
            this.layoutResourceId = layout;
            this.data = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId,parent,false);
            }
            User currentUser = data.get(position);
            Details = (TextView) convertView.findViewById(R.id.simple_spinner_item);
            Details.setText(currentUser.toString());
            return convertView;

        }

    }
}
