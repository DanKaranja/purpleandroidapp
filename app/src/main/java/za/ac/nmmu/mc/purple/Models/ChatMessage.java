package za.ac.nmmu.mc.purple.Models;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.UUID;

import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

/**
 * Created by danie on 5/6/2016.
 */
public class ChatMessage implements Serializable{
    public String MessageID ;
    private String Message ;
    public String UserID ;
    public User FromUser;
    public String HotspotID ;
    public String CreationDate ;
    public int Type;

    public ChatMessage()
    {

    }

    public ChatMessage(String Message, String UserID, String HotspotID, String CreationDate, int Type)
    {
        this.MessageID = UUID.randomUUID().toString();
        this.UserID = UserID;
        this.HotspotID = HotspotID;
        this.CreationDate = CreationDate;
        this.Type = Type;
        switch (this.Type){
            case 0:
                this.Message = AppToolbox.Encode64(Message);
                break;
            case 1:
                this.Message = Message;
        }

    }
    public Bitmap getImgMessage(){
        if(Type == 1){
            return AppToolbox.decodeImgBase64(Message);
        }
        else return null;
    }

    public String getMessage() {
        if(Type == 0)
            return AppToolbox.Decode64(Message);
        else return null;
    }


}
