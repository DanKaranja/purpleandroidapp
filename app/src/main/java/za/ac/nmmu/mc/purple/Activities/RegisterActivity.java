package za.ac.nmmu.mc.purple.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.Models.User;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

public class RegisterActivity extends AppCompatActivity {
    private EditText _nameText;
    private EditText _phoneText;
    private Button _signupButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _nameText = (EditText) findViewById(R.id.signup_input_name);
        _phoneText = (EditText) findViewById(R.id.signup_input_phone);
        _signupButton = (Button) findViewById(R.id.signup_Btn);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });
    }
    public void signup() {
        if (!validate()) {
            SuperToast.create(getBaseContext(), getResources().getString(R.string.failed_signup_invalid_details), SuperToast.Duration.LONG,
                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
            return;
        }

        String name = _nameText.getText().toString();
        String phone = _phoneText.getText().toString();

        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this,R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Performing some mojo...");
        progressDialog.show();
        _signupButton.setEnabled(false);

        final User user = new User(name,phone);
        AppToolbox.service.registerUser(user)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                        _signupButton.setEnabled(true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.dismiss();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.failed_signup_generic), SuperToast.Duration.LONG,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();

                    }

                    @Override
                    public void onNext(PResponse pResponse) {
                        if(pResponse.getCheck())
                        {
                            SharedPreferences account = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                            SharedPreferences.Editor editor = account.edit();
                            editor.putString("name", user.getFullName());
                            editor.putString("phone", user.CellNumber);
                            editor.commit();
                            onSignupSuccess(user);
                        }
                        else
                            SuperToast.create(getBaseContext(), pResponse.getResponseString(), SuperToast.Duration.LONG,
                                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                    }
                });
    }

    public void onSignupSuccess(User user) {
        AppToolbox.user = user;
        //Toast.makeText(getBaseContext(),"Signup was successful. Logging in as "+user.FullName,Toast.LENGTH_LONG).show();
        SuperToast.create(getBaseContext(),"Signup was successful. Logging in as "+user.getFullName(), SuperToast.Duration.LONG,
                Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
        Intent MAintent = new Intent(this, MainActivity.class);
        MAintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(MAintent);
        finish();
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String phone = _phoneText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("At least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (phone.length() != 10) {
            _phoneText.setError("Enter a valid South African phone number");
            valid = false;
        } else {
            _phoneText.setError(null);
        }

        return valid;
    }
}
