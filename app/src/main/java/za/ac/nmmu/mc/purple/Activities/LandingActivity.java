package za.ac.nmmu.mc.purple.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NavUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Models.User;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;


public class LandingActivity extends Activity implements View.OnClickListener{

    private Button _newAccButton;
    private Button _retry_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);
        _newAccButton = (Button) findViewById(R.id.action_sign_up_btn);
        _retry_login = (Button) findViewById(R.id.action_retry_login_btn);

        _newAccButton.setOnClickListener(this);
        _retry_login.setOnClickListener(this);

        Intent landingIntent = getIntent();
        int landingType = landingIntent.getIntExtra("LNDTYPE",-1);
        switch (landingType){
            case -1:
                signIn();
                break;
            case 1://Account removed
                Snackbar.make(findViewById(R.id.content_controls_landing), R.string.general_failure, Snackbar.LENGTH_LONG).show();
                break;
            case 2://Account removed
                Snackbar.make(findViewById(R.id.content_controls_landing), R.string.logout_landing, Snackbar.LENGTH_LONG).show();
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case(R.id.action_retry_login_btn):
                signIn();
                break;
            case (R.id.action_sign_up_btn):
                Intent SUintent = new Intent(this, RegisterActivity.class);
                startActivity(SUintent);
                break;
        }
    }

    public void signIn() {

        _retry_login.setEnabled(false);
        _newAccButton.setEnabled(false);
        final ProgressDialog progressDialog = new ProgressDialog(LandingActivity.this,R.style.AppTheme_Dark_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        SharedPreferences account = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String name = account.getString("name", null);
        String phone = account.getString("phone", null);

        if((name != null) && (phone != null))
        {
            AppToolbox.service.loginUser(new String[]{AppToolbox.Encode64(name), phone})
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<User>() {
                        @Override
                        public void onCompleted() {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            progressDialog.dismiss();
                            SuperToast.create(getBaseContext(),e.getMessage(), SuperToast.Duration.LONG,
                                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                            onLoginFailed(-3);

                        }

                        @Override
                        public void onNext(User user) {
                            if (user.UserID != null)
                                onLoginSuccess(user);
                            else
                                onLoginFailed(-2);
                        }
                    });
        }
        else {
            progressDialog.dismiss();
            onLoginFailed(-1);
        }
    }

    public void onLoginSuccess(User user) {
        //Toast.makeText(getBaseContext(),"Successfully logged in as "+user.FullName,Toast.LENGTH_LONG).show();
        SuperToast.create(getBaseContext(),"Successfully logged in as "+user.getFullName(), SuperToast.Duration.LONG,
                Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
        Intent MAintent = new Intent(this, MainActivity.class);
        AppToolbox.user = user;
        startActivity(MAintent);
        finish();
    }

    public void onLoginFailed(int error) {
        switch (error) {
            case -1:
                Snackbar.make(findViewById(R.id.content_controls_landing), R.string.failed_login_account, Snackbar.LENGTH_LONG).show();
                break;
            case -2:
                Snackbar.make(findViewById(R.id.content_controls_landing), R.string.failed_login_user, Snackbar.LENGTH_LONG).show();
                break;
            case -3:
                Snackbar.make(findViewById(R.id.content_controls_landing), R.string.failed_login_connection, Snackbar.LENGTH_LONG).show();
                break;
        }
        _retry_login.setEnabled(true);
        _newAccButton.setEnabled(true);
    }


}
