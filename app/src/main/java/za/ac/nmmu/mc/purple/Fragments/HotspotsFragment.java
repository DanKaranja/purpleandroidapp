package za.ac.nmmu.mc.purple.Fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.dexafree.materialList.card.Card;
import com.dexafree.materialList.card.CardProvider;
import com.dexafree.materialList.card.OnActionClickListener;
import com.dexafree.materialList.card.action.TextViewAction;
import com.dexafree.materialList.card.action.WelcomeButtonAction;
import com.dexafree.materialList.view.MaterialListView;
import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;
import com.squareup.picasso.RequestCreator;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Activities.AddHotspotActivity;
import za.ac.nmmu.mc.purple.Activities.ChatActivity;
import za.ac.nmmu.mc.purple.Activities.HotspotDetailsActivity;
import za.ac.nmmu.mc.purple.Activities.LandingActivity;
import za.ac.nmmu.mc.purple.Activities.MainActivity;
import za.ac.nmmu.mc.purple.Models.Hotspot;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.Models.User;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

/**
 * Created by danie on 4/25/2016.
 */
public class HotspotsFragment extends Fragment {
    private boolean FreshPull = true;
    private MaterialListView mListView;
    private SwipeRefreshLayout swipeContainer;
    public ProgressDialog progressDialog;

    public HotspotsFragment() {
    }
    public static HotspotsFragment newInstance() {
        HotspotsFragment fragment = new HotspotsFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_hotspots, container, false);

        FloatingActionButton fab  = (FloatingActionButton) rootView.findViewById(R.id.hotspot_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent AddClassIntent = new Intent(getActivity().getBaseContext(), AddHotspotActivity.class);
                startActivity(AddClassIntent);
            }
        });

        mListView = (MaterialListView) rootView.findViewById(R.id.hotspot_material_listview);
        progressDialog = new ProgressDialog(getActivity(),R.style.AppTheme_Dark_Dialog);
        swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.hotspot_swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                GetHotspotList();
            }
        });
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        if(AppToolbox.user!=null)
            GetHotspotList();
        return rootView;
    }
    private void showAsynchDialog(String message){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
    private void dismissAsynchDialog(){
        progressDialog.dismiss();
    }
    private void GetHotspotList(){
        if(FreshPull)
            showAsynchDialog("Fetching....");
        swipeContainer.setRefreshing(true);
        AppToolbox.service.getHotspots(AppToolbox.user.UserID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ArrayList<Hotspot>>() {
                    @Override
                    public void onCompleted() {
                        swipeContainer.setRefreshing(false);
                        dismissAsynchDialog();
                        FreshPull = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        swipeContainer.setRefreshing(false);
                        SuperToast.create(getActivity().getBaseContext(),getResources().getString(R.string.general_failure), SuperToast.Duration.LONG,
                                Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();
                    }

                    @Override
                    public void onNext(ArrayList<Hotspot> hotspotList) {
                        mListView.getAdapter().clearAll();
                        if (hotspotList.size() != 0) {
                            UpdateList(hotspotList);
                        } else {
                            SuperToast.create(getActivity().getBaseContext(),getResources().getString(R.string.alert_empty_hotspotlist), SuperToast.Duration.LONG,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                        }
                    }
                });

    }

    public void UpdateList(ArrayList<Hotspot> hotspotList){

        for(int i = 0; i < hotspotList.size(); i++)
        {
            final Hotspot currentHotspot = hotspotList.get(i);
            String Title = currentHotspot.getHotspotQuestion();
            String Description = currentHotspot.getHotspotDescription();
            if(Title.length() > 60) {
                Title = Title.substring(0, 60).concat("...");
            }
            if(Description.length() > 100) {
                Description = Description.substring(0, 100).concat("...");
            }
            Card card = new Card.Builder(getContext())
                    .withProvider(new CardProvider())
                    .setLayout(R.layout.material_basic_buttons_card)
                    .setTitle(Title)
                    .setDescription(Description)
                    .addAction(R.id.left_text_button, new TextViewAction(getContext())
                            .setText("Details")
                            .setTextResourceColor(R.color.primary)
                            .setListener(new OnActionClickListener() {
                                @Override
                                public void onActionClicked(View view, Card card) {
                                    Intent DetailsIntent = new Intent(getActivity().getBaseContext(), HotspotDetailsActivity.class);
                                    DetailsIntent.putExtra("CH", currentHotspot);
                                    startActivity(DetailsIntent);
                                }
                            }))
                    .addAction(R.id.right_text_button, new TextViewAction(getContext())
                            .setText("View Thread")
                            .setTextResourceColor(R.color.accent)
                            .setListener(new OnActionClickListener() {
                                @Override
                                public void onActionClicked(View view, Card card) {
                                    Intent ChatIntent = new Intent(getActivity().getBaseContext(), ChatActivity.class);
                                    AppToolbox.CHID = currentHotspot.HotspotID;
                                    startActivity(ChatIntent);
                                }
                            }))
                    .endConfig()
                    .build();
            mListView.getAdapter().add(card);
        }

    }



}


