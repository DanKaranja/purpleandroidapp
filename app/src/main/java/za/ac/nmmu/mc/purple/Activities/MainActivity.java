package za.ac.nmmu.mc.purple.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.widget.Toast;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Fragments.ClassesFragment;
import za.ac.nmmu.mc.purple.Fragments.HotspotsFragment;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.Models.User;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private RemoveAccountFragment removeAccountFragment;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private User user = AppToolbox.user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);  // clear all scroll flags
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.main_container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        Intent activityIntent = getIntent();
        if((activityIntent == null) || (user == null))//if something went horribly wrong.
            logout(1);
        switch (activityIntent.getIntExtra("WUWMTD",0)){ //Tell child activities want it to do.
            case 2:
                mViewPager.setCurrentItem(1,true);
                break;
            case 1:
                mViewPager.setCurrentItem(0,true);
                break;
        }
    }
    private void logout(int LNDTYPE)
    {
        Intent landingIntent = new Intent(this, LandingActivity.class);
        landingIntent.putExtra("LNDTYPE", LNDTYPE);
        landingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(landingIntent);
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            removeAccountFragment = new RemoveAccountFragment();
            removeAccountFragment.show(getSupportFragmentManager(), "LEAVE");
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 1:
                    return ClassesFragment.newInstance();
            }
            return HotspotsFragment.newInstance();
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "HOTSPOTS";
                case 1:
                    return "CLASSES";
            }
            return null;
        }
    }

    @SuppressLint("ValidFragment")
    public class RemoveAccountFragment extends DialogFragment {

        public RemoveAccountFragment(){
        }
        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);

        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),R.style.AppTheme_Dark_Dialog);
            builder.setTitle("Removing account...");
            builder.setMessage("Are you sure you would like to remove this account? Doing so will purge all your Hotspots along with your soul.")
                    .setPositiveButton(R.string.action_general_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            RemoveAccount();
                        }
                    })
                    .setNegativeButton(R.string.action_general_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                        }
                    });
            return builder.create();
        }
        private void RemoveAccount(){
            final ProgressDialog progressDialog = new ProgressDialog(MainActivity.this,R.style.AppTheme_Dark_Dialog);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Removing...");
            progressDialog.show();

            AppToolbox.service.deleteUser(user.UserID)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<PResponse>() {
                        @Override
                        public void onCompleted() {
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onError(Throwable e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                            SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), SuperToast.Duration.LONG,
                                    Style.getStyle(Style.RED, SuperToast.Animations.FLYIN)).show();

                        }

                        @Override
                        public void onNext(PResponse pResponse) {
                            if (pResponse.getCheck())//For some reason these responses are negated
                                logout(2);
                            else
                                SuperToast.create(getBaseContext(), pResponse.getResponseString(), SuperToast.Duration.LONG,
                                        Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                        }
                    });

        }
    }
}
