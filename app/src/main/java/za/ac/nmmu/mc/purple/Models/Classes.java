package za.ac.nmmu.mc.purple.Models;

import java.io.Serializable;
import java.util.UUID;

import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

/**
 * Created by danie on 4/27/2016.
 */
public class Classes implements Serializable{
    public String ClassID;
    private String ClassName;
    private String ClassInstitution;
    private String ClassDescription;
    public String CountUserIDs;

    public Classes()
    {

    }
    public Classes(String ClassName,String Institution, String ClassDescription) {
        this.ClassID = UUID.randomUUID().toString();
        this.ClassName = ClassName;
        this.ClassInstitution = Institution;
        this.ClassDescription = AppToolbox.Encode64(ClassDescription);
    }

    @Override
    public String toString() {
        return getClassName()+"@("+getClassInstitution()+") | Students: "+CountUserIDs+"";
    }

    public String getClassName() {
        return ClassName;
    }


    public String getClassInstitution() {
        return ClassInstitution;
    }


    public String getClassDescription() {
        return AppToolbox.Decode64(ClassDescription);
    }

}
