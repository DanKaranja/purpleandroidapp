package za.ac.nmmu.mc.purple.WebApi;


import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import za.ac.nmmu.mc.purple.Models.ChatMessage;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.User;

/**
 * Created by danie on 4/24/2016.
 */
public class AppToolbox  {
    private static String NMMUBASEURL = "http://purplewebapi.csdev.nmmu.ac.za/api/";
    private static String TESTBASEURL = "http://10.0.0.3:59332/api/";
    public static User user;
    public static String CHID = null; //Current Hotspot ID
    public static HashMap<String,ArrayList<ChatMessage>> CHATCACHE = new HashMap<>();
    public static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(NMMUBASEURL)
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    public static RestInterface service = retrofit.create(RestInterface.class);

    public static String Encode64(String string){
        if(string != null) {
            byte[] data = new byte[0];
            try {
                data = string.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return Base64.encodeToString(data, Base64.DEFAULT);
        }
        return null;
    }

    public static String Decode64(String base64Str){
        if(base64Str != null) {
            byte[] data = Base64.decode(base64Str, Base64.DEFAULT);
            String string = "Decode Unsuccessful";
            try {
                string = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            return string;
        }
        else
            return null;
    }

    public static String encodeImgToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality)
    {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeImgBase64(String input)
    {
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

}
