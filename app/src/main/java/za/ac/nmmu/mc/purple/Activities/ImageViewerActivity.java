package za.ac.nmmu.mc.purple.Activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

import uk.co.senab.photoview.PhotoViewAttacher;
import za.ac.nmmu.mc.purple.Models.ChatMessage;
import za.ac.nmmu.mc.purple.R;

public class ImageViewerActivity extends AppCompatActivity {

    private ImageView _imageViewer;
    PhotoViewAttacher mAttacher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_viewer);
        Intent activityIntent = getIntent();
        if(activityIntent == null)
            finish();
        ChatMessage Message = (ChatMessage) activityIntent.getSerializableExtra("MSG");
        if(Message == null)
            finish();
        _imageViewer = (ImageView) findViewById(R.id.imageViewer);
        _imageViewer.setImageBitmap(Message.getImgMessage());
        mAttacher = new PhotoViewAttacher(_imageViewer);
    }
}
