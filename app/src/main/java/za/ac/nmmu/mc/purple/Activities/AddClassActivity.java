package za.ac.nmmu.mc.purple.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.github.johnpersano.supertoasts.SuperToast;
import com.github.johnpersano.supertoasts.util.Style;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import za.ac.nmmu.mc.purple.Fragments.FragmentListener;
import za.ac.nmmu.mc.purple.Models.Classes;
import za.ac.nmmu.mc.purple.Models.PResponse;
import za.ac.nmmu.mc.purple.R;
import za.ac.nmmu.mc.purple.WebApi.AppToolbox;

public class AddClassActivity extends AppCompatActivity implements FragmentListener {
    public String JoinClassID = null;
    public TextView _create_class_name;
    public TextView _create_class_institution;
    public JoinClassDialogFragment ExistingClassDialog;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_class);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        _create_class_institution = (TextView) findViewById(R.id.join_class_institution);
        ExistingClassDialog = new JoinClassDialogFragment();
        _create_class_name = (TextView) findViewById(R.id.join_class_name);
        progressDialog = new ProgressDialog(AddClassActivity.this,R.style.AppTheme_Dark_Dialog);
        getFragmentManager().beginTransaction().add(R.id.creat_class_framgnet_container, ClassSearchFragment.newInstance(this)).commit();


    }
    private void showAsynchDialog(String message){
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage(message);
        progressDialog.show();
    }
    private void dismissAsynchDialog(){
        progressDialog.dismiss();
    }

    private void swapFragments(){
        getFragmentManager().beginTransaction().replace(R.id.creat_class_framgnet_container, CreateJoinFragment.newInstance(this)).commit();
    }
    private void goBackToMain(){
        Intent MAintent = new Intent(this, MainActivity.class);
        MAintent.putExtra("WUWMTD",2);
        startActivity(MAintent);
        finish();
    }
    private void startJoinClass(){
        JoinClass(new String[]{AppToolbox.user.UserID, JoinClassID});
    }

    //LISTENERS
    @Override
    public void onFragmentOptionClick(int option, String FLAG){
        if(FLAG == "CSF"){
            switch (option){
                case 1:
                    if(validate())
                        SearchforClass(new String[]{_create_class_name.getText().toString(),_create_class_institution.getText().toString()});
                    break;
            }
        }
        if(FLAG == "JCDF"){
            switch (option){
                case 1:
                    startJoinClass();
                    break;
                case 0:
                    ExistingClassDialog.dismiss();
                    break;
            }
        }
    }
    @Override
    public void onFragmentObjectClick(int option, Object object, String FLAG) {
        if(FLAG == "CJF"){
            switch (option){
                case 1: {
                    if(validate()) {
                        Classes newClass = new Classes(_create_class_name.getText().toString(), _create_class_institution.getText().toString(), (String) object);//ojbect is the description sent from the fragment
                        JoinClassID = newClass.ClassID;
                        CreateClass(newClass);
                    }
                    break;
                }
            }
        }
    }

    public boolean validate() {
        boolean valid = true;

        String classname = _create_class_name.getText().toString();
        String classinstitution = _create_class_institution.getText().toString();

        if (classname.isEmpty()) {
            _create_class_name.setError("Class name cannot be empty");
            valid = false;
        } else {
            _create_class_name.setError(null);
        }

        if (classinstitution.isEmpty()) {
            _create_class_institution.setError("Please enter a valid institution");
            valid = false;
        } else {
            _create_class_institution.setError(null);
        }

        return valid;
    }

    //WEB API CALLS
    private void JoinClass(String[] classUserDetails) {
        showAsynchDialog("Joining class...");
        AppToolbox.service.joinClass(classUserDetails)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();

                    }
                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 500,
                                Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                    }

                    @Override
                    public void onNext(PResponse pResponse) {
                        if (pResponse.getCheck()) {
                            SuperToast.create(getBaseContext(), pResponse.getResponseString(), 1500,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            goBackToMain();
                        } else {
                            SuperToast.create(getBaseContext(), pResponse.getResponseString(), 1500,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                        }
                    }
                });
    }
    private void CreateClass(Classes newClass){
        showAsynchDialog("Creating new Class...");
        AppToolbox.service.createClass(newClass)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();
                        startJoinClass();

                    }
                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 500,
                                Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                    }
                    @Override
                    public void onNext(PResponse pResponse) {
                        SuperToast.create(getBaseContext(), pResponse.getResponseString(), 1500,
                                Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                    }
                });
    }
    private void SearchforClass(String[] classDetails){
        showAsynchDialog("Searching for class...");
        AppToolbox.service.searchClass(classDetails)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PResponse>() {
                    @Override
                    public void onCompleted() {
                        dismissAsynchDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        dismissAsynchDialog();
                        SuperToast.create(getBaseContext(), getResources().getString(R.string.general_failure), 500,
                                Style.getStyle(Style.RED, SuperToast.Animations.POPUP)).show();
                    }

                    @Override
                    public void onNext(PResponse pResponse) {
                        if (pResponse.getCheck()) {
                            JoinClassID = pResponse.getResponseString();
                            ExistingClassDialog.show(getSupportFragmentManager(),"EXISTINGCLASS");
                        }
                        else
                        {
                            SuperToast.create(getBaseContext(), pResponse.getResponseString(), 1500,
                                    Style.getStyle(Style.BLUE, SuperToast.Animations.FLYIN)).show();
                            swapFragments();
                        }
                    }
                });
    }

    //FRAGMENTS
    public static class ClassSearchFragment extends Fragment {
        private static FragmentListener mListener;

        public static ClassSearchFragment newInstance(Activity activity) {
            ClassSearchFragment fragment = new ClassSearchFragment();
            try {
                mListener = (FragmentListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement FragmentListener");
            }
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View view =  inflater.inflate(R.layout.class_search_fragment, container, false);
            Button _class_search_Btn = (Button) view.findViewById(R.id.class_search_Btn);

            _class_search_Btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFragmentOptionClick(1, "CSF");
                }
            });
            return view;
        }
    }
    public static class CreateJoinFragment extends Fragment {
        private static FragmentListener mListener;
        public static CreateJoinFragment newInstance(Activity activity) {
            CreateJoinFragment fragment = new CreateJoinFragment();
            try {
                mListener = (FragmentListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement FragmentListener");
            }
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            // Inflate the layout for this fragment
            View view =  inflater.inflate(R.layout.class_create_join_fragment, container, false);
            final EditText _create_class_description = (EditText) view.findViewById(R.id.create_class_description);
            Button _class_create_Btn = (Button) view.findViewById(R.id.class_create_Btn);
            _class_create_Btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFragmentObjectClick(1, _create_class_description.getText().toString(), "CJF");
                }
            });

            return view;
        }
    }

    //POPUP DIALOG
    @SuppressLint("ValidFragment")
    public class JoinClassDialogFragment extends DialogFragment {
        private FragmentListener mListener;
        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            try {
                mListener = (FragmentListener) activity;
            } catch (ClassCastException e) {
                throw new ClassCastException(activity.toString() + " must implement FragmentListener");
            }
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AddClassActivity.this,R.style.AppTheme_Dark_Dialog);
            builder.setTitle("Join Class?");
            builder.setMessage(R.string.info_class_exists)
                    .setPositiveButton(R.string.action_general_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mListener.onFragmentOptionClick(1, "JCDF");
                        }
                    })
                    .setNegativeButton(R.string.action_general_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            mListener.onFragmentOptionClick(0, "JCDF");
                        }
                    });
            return builder.create();
        }
    }

}
