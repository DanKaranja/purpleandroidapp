package za.ac.nmmu.mc.purple.Models;

/**
 * Created by danie on 4/24/2016.
 */
public class PResponse {
    public int check;
    public String responseString;

    public PResponse(int check, String responseString) {
        this.check = check;
        this.responseString = responseString;
    }

    public boolean getCheck() {
        return (check == 1);
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public String getResponseString() {
        return responseString;
    }

    public void setResponseString(String responseString) {
        this.responseString = responseString;
    }
}
